#! /bin/bash
# Create symbolic links between $HOME/ and dotfiles/ for personal configuration files like .bahrc, .vimrc ...

createLink () {
	TARGET="$TARGET_FOLDER/$1"
	LINK="$LINK_FOLDER/.$1"
	[[ -f "$TARGET" ]] || { echo "ERROR: $TARGET is not present"; return 1; }
	[[ -f "$LINK" ]] && mv "$LINK"{,.bak}
	ln -s $TARGET $LINK && echo "Created symlink for $1"
}

FILES=('bashrc' 'bash_aliases' 'vimrc' 'git_aliases')
TARGET_FOLDER="$( cd "$(dirname "$0")" ; pwd -P )"
LINK_FOLDER=$HOME

for file in "${FILES[@]}" ; do
	 createLink $file
done

exit 0
