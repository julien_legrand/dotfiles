""""""""""""""
"   .vimrc   "
""""""""""""""

" General
set nocompatible
filetype plugin indent on
syntax enable 
"OPTIONS
set spelllang=fr
set mouse=a
set bg=dark
set ignorecase "Ignor case durin search 
set smartcase
set incsearch
set hlsearch
set scrolloff=3
set splitright " New window on right
set splitbelow "New window below
set clipboard=unnamedplus " copy/paste from system clipbord, if not working -> install gvim (about compilation options...)
set path+=** " fuzzy search whit completion, :find
set wildmenu "?
set tabstop=4 " tab = 4 spaces
set shiftwidth=4 " tab indentation = 4 spaces 
"set expandtab " remplace les tab par des espaces
set hidden " for coc plugin, change buffer without saving
set nobackup 
set nowritebackup
set noswapfile 
set laststatus=2 " status line
set cmdheight=2 " Give more space for displaying messages.
set updatetime=300
set nu
set rnu
set foldmethod=marker " could be replaced by foldmethod=syntax or indent
packadd! matchit
" COLOR
" cterm for color terminal (256 colors)
" replace cterm by gui for a GUI
"hi Normal ctermbg=black
"set colorcolumn=80 " Coloratoin de la 80ème colonne
"hi ColorColumn ctermbg=236 guibg=#303030 "Couleur de la colonne
set cursorline "mise ne évidence de la ligne du curseur
hi CursorLine cterm=NONE ctermbg=237 guibg=#3a3a3a "Ligne en cours, pas de soulignage
hi VertSplit  cterm=NONE ctermbg=237 guibg=#3a3a3a "Ligne de séparation verticale 
hi StatusLineNC  cterm=NONE ctermbg=237 guibg=#3a3a3a "Ligne de séparation horizontale 
hi CursorLineNr cterm=NONE 
hi LineNr ctermfg=grey guifg=grey "Couleur des numéros de lignes
" Only in the current window
augroup CursorLine
  au!
  au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  au WinLeave * setlocal nocursorline
augroup END
" Change color between Insert mode or Normal mode
autocmd InsertEnter * hi CursorLine ctermbg=17 guifg=#00005f
autocmd InsertLeave * hi CursorLine ctermbg=237 guibg=#3a3a3a

" Mapping
let mapleader = " "
nnoremap <leader>t :tabnew<CR>
nnoremap <leader>v :vsplit<CR>
nnoremap <leader>no :nohlsearch<CR>
"imap jk <Esc>
"nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <leader>e :Vex<CR>
" Naviguer entre windows avec Ctrl-h,j,k,l
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" Correciont orthographe
nnoremap <leader>f ]s
nnoremap <leader>b [s
nnoremap <leader>d z=

" Easymotion
let g:EasyMotion_keys = 'asdghklqwertyuiopzxcvbnmfj'
map <Leader><Leader>w <Plug>(easymotion-bd-w)
map <Leader><Leader>w <Plug>(easymotion-overwin-w)

" Vim plug
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin()
	Plug 'vimwiki/vimwiki'
	Plug 'alvan/vim-closetag'
	Plug 'tpope/vim-surround'
	Plug 'tpope/vim-repeat'
	Plug 'tpope/vim-commentary'
	Plug 'preservim/nerdtree'
	Plug 'easymotion/vim-easymotion'
call plug#end()

" VimWiki
"Repertoire par défaut pour vimwiki + markdown
let g:vimwiki_list = [{'path': '~/vimwiki',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

" netrw
"let g:netrw_banner=0        " disable annoying banner
"let g:netrw_browse_split=4  " open in prior window
let g:netrw_browse_split=2  " open in vertical split
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
let g:netrw_winsize=30 "small ?

" WSL yank support
let s:clip = '/mnt/c/Windows/System32/clip.exe'  " change this path according to your mount point
if executable(s:clip)
    augroup WSLYank
        autocmd!
        autocmd TextYankPost * if v:event.operator ==# 'y' | call system(s:clip, @0) | endif
    augroup END
endif
