# What are dotfiles ?
Dotfiles are the customization files (their filenames usually begin with a period) that are used to personalize your linux or other Unix-based system. This repository contains my personal dotfiles.

This repository is mainly about bash, vim and git configurations and aliases.

# How to use it ?
These settings are very personal and not for everyone, so be sure of what you are doing before using them.

I use mainly Debian based distributions, you will need to make some adjustments for other distributions.

Download with git :

	git clone https://gitlab.com/julien_legrand/dotfiles.git

Run the interactive `setup.sh` script which will offer you to create symlink and install dependencies.

	cd dotfiles && ./setup.sh

# License
The files and scripts in this repository are licensed under the MIT License,
