#! /bin/bash

SCRIPT_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
SRC_DIR="$SCRIPT_DIR/src"
CLI_PKG_LIST="$SRC_DIR/aptList_cli"
GUI_PKG_LIST="$SRC_DIR/aptList_gui"
LAPTOP_PKG_LIST="$SRC_DIR/aptList_laptop"
BASIC_PKG="vim git curl bash-completion"

installVimPlugins () {
	echo "Installing Vim Plug"
	curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	echo "Installing vim plugin" && \
	vim -c PlugInstall -c q -c q
}

cloneVimwiki () {
	read  -p "Do you want to clone using ssh key ?  [Y/n] " -n 1 -r Q_USE_SSH_KEY
	Q_USE_SSH_KEY=${Q_USE_SSH_KEY:-Y}
	echo
   	if [[ Q_USE_SSH_KEY == [YyOo] ]];
		then 
			GIT_URL="git@gitlab.com:julien_legrand/"
			git clone "${GIT_URL}vimwiki.git" 1> /dev/null
		else
			read -p "Enter your git name (default:julien_legrand) :" GIT_NAME
		   	GIT_NAME=${GIT_NAME:-"julien_legrand"}
			echo
			read -sp "Enter your git password :" GIT_PASS
			GIT_URL="https://$GIT_NAME:$GIT_PASS@gitlab.com/$GIT_NAME/"
			echo
			echo "Cloning vimwiki git repos..."
			cd
			git clone "${GIT_URL}vimwiki.git" 1> /dev/null
			cd -
	fi
}

installFromList () {
	apt install $(cat $1)
}

writeNetTemplate () {
	cat $SRC_DIR/template_debian_interfaces >> /etc/network/interfaces
}

# Introduction
echo "**************************************************"
echo "****************** Welcome !! ********************"
echo "**************************************************"
echo
echo "You may need to install these packages before : "
echo "$BASIC_PKG"
echo

# User interaction
if [[ $(id -u) = 0 ]]; # user is root -> more features
	then 
		read  -p "Do you want to install this packages? [Y/n] " -n 1 -r Q_INSTALL_BASICS_PKGS
		Q_INSTALL_BASICS_PKGS=${Q_INSTALL_BASICS_PKGS:-Y}
		echo
		read  -p "Do you want to install CLI packages? [Y/n] " -n 1 -r Q_INSTALL_CLI_PKGS
		Q_INSTALL_CLI_PKGS=${Q_INSTALL_CLI_PKGS:-Y}
		echo
		read  -p "Do you want to install GUI packages? [Y/n] " -n 1 -r Q_INSTALL_GUI_PKGS
		Q_INSTALL_GUI_PKGS=${Q_INSTALL_GUI_PKGS:-Y}
		echo
		read  -p "Do you want to install laptop packages? [Y/n] " -n 1 -r Q_INSTALL_LAPTOP_PKGS
		Q_INSTALL_LAPTOP_PKGS=${Q_INSTALL_LAPTOP_PKGS:-Y}
		echo
		read  -p "Do you want to write some basic configuration examples to /etc/network/interfaces ?[Y/n] " -n 1 -r Q_NET_TEMPLATE
		Q_NET_TEMPLATE=${Q_NET_TEMPLATE:-Y}
		echo
fi

read  -p "Do you want to install vim plugins ? [Y/n] " -n 1 -r Q_INSTALL_VIM_PLUGINS
Q_INSTALL_VIM_PLUGINS=${Q_INSTALL_VIM_PLUGINS:-Y}
echo
read  -p "Do you want to clone your vimwiki ?[Y/n] " -n 1 -r Q_CLONE_VIMWIKI
Q_CLONE_VIMWIKI=${Q_CLONE_VIMWIKI:-Y}
echo
read  -p "Do you want to create symlinks for your dotfiles ?[Y/n] " -n 1 -r Q_CREATE_SYMLINKS_DOTFILES
Q_CREATE_SYMLINKS_DOTFILES=${Q_CREATE_SYMLINKS_DOTFILES:-Y}
echo

# Proceed

[[ $Q_INSTALL_BASICS_PKGS == [YyOo] ]] && apt install $BASIC_PKG
[[ $Q_INSTALL_CLI_PKGS == [YyOo] ]] && installFromList $CLI_PKG_LIST
[[ $Q_INSTALL_GUI_PKGS == [YyOo] ]] && installFromList $GUI_PKG_LIST
[[ $Q_INSTALL_LAPTOP_PKGS == [YyOo] ]] && installFromList $LAPTOP_PKG_LIST
[[ $Q_NET_TEMPLATE == [YyOo] ]] && writeNetTemplate

[[ $Q_INSTALL_VIM_PLUGINS == [YyOo] ]] && installVimPlugins
[[ $Q_CREATE_SYMLINKS_DOTFILES == [YyOo] ]] && $SRC_DIR/linkMyDotfiles.sh
[[ $Q_CLONE_VIMWIKI == [YyOo] ]] && cloneVimwiki

echo "************************ END **************************"
echo

exit 0
